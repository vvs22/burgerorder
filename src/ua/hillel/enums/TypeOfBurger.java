package ua.hillel.enums;

public enum TypeOfBurger {
    NULL(null, 0.0, 0),
    SMALL("Маленький бургер ", 50.0, 20),
    BIG("Большой бургер ", 100.0, 40);

    private final String nameTypeOfBurger;
    private final double priceOfBurger;
    private final int caloriesOfBurger;

    TypeOfBurger(String nameTypesOfBurger, double priceOfBurger, int caloriesOfBurger) {
        this.nameTypeOfBurger = nameTypesOfBurger;
        this.priceOfBurger = priceOfBurger;
        this.caloriesOfBurger = caloriesOfBurger;
    }

    public String getName() {
        return nameTypeOfBurger;
    }

    public double getPriceOfBurger() {
        return priceOfBurger;
    }

    public int getCaloriesOfBurger() {
        return caloriesOfBurger;
    }

    @Override
    public String toString() {
        return (nameTypeOfBurger == null ? " " : nameTypeOfBurger);
    }
}
