package ua.hillel.enums;

public enum AdditionToBurger {
    NULL(null, 0.0, 0),
    SALAD("листья салата ", 20.0, 5),
    CHEESE("одинарный сыр ", 10.0, 20),
    DOUBLECHEES("двойной сыр ", 20.0, 40),
    POTATO("картофель ", 15.0, 10);

    private final String nameOfAddition;
    private final double priceOfAddition;
    private final int caloriesOfAddition;

    AdditionToBurger(String nameOfAddition, double priceOfAddition, int caloriesOfAddition) {
        this.nameOfAddition = nameOfAddition;
        this.priceOfAddition = priceOfAddition;
        this.caloriesOfAddition = caloriesOfAddition;
    }

    public String getNameOfAdditions() {
        return nameOfAddition;
    }

    public double getPriceOfAdditions() {
        return priceOfAddition;
    }

    public int getCaloriesOfAdditions() {
        return caloriesOfAddition;
    }

    @Override
    public String toString() {
        return (nameOfAddition == null ? " " : nameOfAddition);
    }
}
