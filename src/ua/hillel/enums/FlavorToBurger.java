package ua.hillel.enums;

public enum FlavorToBurger {
    NULL(null, 0.0, 0),
    FLAVOR("Приправа ", 15.0, 0),
    MAYONNAISE("Майонез ", 20.0, 5);

    private final String nameOfFlavor;
    private final double priceOfFlavor;
    private final int caloriesOfFlavor;

    FlavorToBurger(String nameOfFlavors, double priceOfFlavor, int caloriesOfFlavor) {
        this.nameOfFlavor = nameOfFlavors;
        this.priceOfFlavor = priceOfFlavor;
        this.caloriesOfFlavor = caloriesOfFlavor;
    }

    public String getNameOfFlavors() {
        return nameOfFlavor;
    }

    public double getPriceOfFlavor() {
        return priceOfFlavor;
    }

    public int getCaloriesOfFlavor() {
        return caloriesOfFlavor;
    }

    @Override
    public String toString() {
        return (nameOfFlavor == null ? " " : nameOfFlavor);
    }
}
