package ua.hillel.enums;

public enum SortOfBurger {
    NULL_BURGER(null),
    CLASSIC("Классический гамбургер - булочка и котлета "),
    CHEESE("Чизбургер - булочка и котлета "),
    DOUBLECHEESE("Дабл Чизбургер - булочка и котлета "),
    BURGER_WITH_POTATO("Гамбургер с картофелем - булочка и котлета ");

    private final String nameSortOfBurger;

    SortOfBurger(String nameSortOfBurger) {
        this.nameSortOfBurger = nameSortOfBurger;
    }

    public String getNameSortOfBurger() {
        return nameSortOfBurger;
    }

    @Override
    public String toString() {
        return (nameSortOfBurger == null ? " " : nameSortOfBurger);
    }
}
