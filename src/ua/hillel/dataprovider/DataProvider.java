package ua.hillel.dataprovider;

import ua.hillel.enums.AdditionToBurger;
import ua.hillel.enums.FlavorToBurger;
import ua.hillel.enums.SortOfBurger;
import ua.hillel.enums.TypeOfBurger;
import ua.hillel.implementations.*;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class DataProvider {
    ArrayList<ClassicBurger> burgerOrder = new ArrayList<>();
    Random myRandom = new Random();
    Scanner myScanner = new Scanner(System.in);
    int numberOfBurgers = 0;

    public DataProvider() {
    }

    public ArrayList<ClassicBurger> addBurgerToOrder(ArrayList<ClassicBurger> burgerArrayList) {
        burgerArrayList.clear();
        System.out.println("Добро пожаловать в фастфуд!");
        System.out.println("В данной программе Вы можете создать заказ на приготовление Бургеров.");
        numberOfBurgers = inputNumberOfBurgers();
        System.out.println("Ок. Вы собираетесь заказать " + numberOfBurgers + " бургеров.");
        int burgerNumber = 1;
        while (burgerNumber <= numberOfBurgers) {
            int typeOfBurger = 0;
            int sortOfBurger = 0;
            System.out.println("Выберите размер бургера № " + burgerNumber + " :");
            typeOfBurger = inputTypeOfBurger();
            System.out.println((TypeOfBurger.values()[typeOfBurger]));
            System.out.println("Выберите разновидность бургера № " + burgerNumber + " :");
            sortOfBurger = inputSortOfBurger();
            System.out.println((SortOfBurger.values()[sortOfBurger]));
            ArrayList<AdditionToBurger> addsToBurger = new ArrayList<>();
            System.out.println("Выберите дополнения к бургеру (при необхожимости) № " + burgerNumber + " :");
            inputAddsToBurger(addsToBurger);
            ArrayList<FlavorToBurger> flavsToBurger = new ArrayList<>();
            System.out.println("Выберите приправы/соусы к бургеру (при необхожимости) № " + burgerNumber + " :");
            inputFlavsToBurger(flavsToBurger);
            switch (sortOfBurger) {
                case 1:
                    burgerArrayList.add(new ClassicBurger(TypeOfBurger.values()[typeOfBurger], addsToBurger, flavsToBurger));
                    break;
                case 2:
                    burgerArrayList.add(new CheeseBurger(TypeOfBurger.values()[typeOfBurger], addsToBurger, flavsToBurger));
                    break;
                case 3:
                    burgerArrayList.add(new DoubleCheeseBurger(TypeOfBurger.values()[typeOfBurger], addsToBurger, flavsToBurger));
                    break;
                case 4:
                    burgerArrayList.add(new GamBurger(TypeOfBurger.values()[typeOfBurger], addsToBurger, flavsToBurger));
                    break;
                default:
                    burgerArrayList.add(new ClassicBurger(TypeOfBurger.values()[typeOfBurger], addsToBurger, flavsToBurger));
                    break;
            }
            burgerNumber++;
        }
        return burgerArrayList;
    }

    private void inputFlavsToBurger(ArrayList<FlavorToBurger> flavsToBurger) {
        boolean isFlavssToBurger = true;
        int indexOfFlavs = -1;
        do {
            System.out.println("Сейчас достурны следующие приправы/соусы к бюргерам (кроме базовых) :");
            System.out.println("После окончания выбора введите 0");
            for (int i = 1; i < FlavorToBurger.values().length; i++) {
                System.out.println(i + " - " + FlavorToBurger.values()[i].toString());
            }
            if (flavsToBurger.size() > 0) {
                System.out.println("Вы уже добавили: " + flavsToBurger.toString());
            }
            try {
                indexOfFlavs = myScanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Введён неверный символ. Необходимо ввести целое число!");
                myScanner.next();
            }
            if (indexOfFlavs < 0 || indexOfFlavs > FlavorToBurger.values().length) {
                System.out.println("Необходимо ввести приправы/соусы . Номер из списка ");
            }
            if (indexOfFlavs > 0 && indexOfFlavs < FlavorToBurger.values().length) {
                flavsToBurger.add(FlavorToBurger.values()[indexOfFlavs]);
            }
            if (indexOfFlavs == 0) {
                isFlavssToBurger = false;
            }
        } while (isFlavssToBurger);
    }

    private void inputAddsToBurger(ArrayList<AdditionToBurger> addsToBurger) {
        boolean isAddsToBurger = true;
        int indexOfAdds = -1;
        do {
            System.out.println("Сейчас достурны следующие дополнения к бюргерам (кроме базовых) :");
            System.out.println("После окончания выбора введите 0");
            for (int i = 1; i < AdditionToBurger.values().length; i++) {
                System.out.println(i + " - " + AdditionToBurger.values()[i].toString());
            }
            if (addsToBurger.size() > 0) {
                System.out.println("Вы уже добавили: " + addsToBurger.toString());
            }
            try {
                indexOfAdds = myScanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Введён неверный символ. Необходимо ввести целое число!");
                myScanner.next();
            }
            if (indexOfAdds < 0 || indexOfAdds > AdditionToBurger.values().length) {
                System.out.println("Необходимо ввести добавки к бургерам . Номер из списка ");
            }
            if (indexOfAdds > 0 && indexOfAdds < AdditionToBurger.values().length) {
                addsToBurger.add(AdditionToBurger.values()[indexOfAdds]);
            }
            if (indexOfAdds == 0) {
                isAddsToBurger = false;
            }
        } while (isAddsToBurger);
        System.out.println(addsToBurger.toString());
    }

    public int inputSortOfBurger() {
        boolean isSortOfBurger = true;
        int indexSortOfBurger = -1;
        do {
            System.out.println("Сейчас достурны следующие виды бюргеров:");
            for (int i = 1; i < SortOfBurger.values().length; i++) {
                System.out.println(i + " - " + SortOfBurger.values()[i].toString());
            }
            try {
                indexSortOfBurger = myScanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Введён неверный символ. Необходимо ввести целое число!");
                myScanner.next();
            }
            if (indexSortOfBurger < 0 || indexSortOfBurger > SortOfBurger.values().length) {
                System.out.println("Необходимо ввести вид бургера . Номер из списка ");
            }
            if (indexSortOfBurger > 0 && indexSortOfBurger < SortOfBurger.values().length) {
                isSortOfBurger = false;
            }
        } while (isSortOfBurger);
        return indexSortOfBurger;
    }

    public int inputTypeOfBurger() {
        boolean isTypeOfBurger = true;
        int indexTypeOfBurger = -1;
        do {
            System.out.println("Сейчас достурны следующие размеры бюргеров:");
            for (int i = 1; i < TypeOfBurger.values().length; i++) {
                System.out.println(i + " - " + TypeOfBurger.values()[i].toString());
            }
            try {
                indexTypeOfBurger = myScanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Введён неверный символ. Необходимо ввести целое число!");
                myScanner.next();
            }
            if (indexTypeOfBurger < 0 || indexTypeOfBurger > TypeOfBurger.values().length) {
                System.out.println("Необходимо ввести размер бургера . Номер из списка ");
            }
            if (indexTypeOfBurger > 0 && indexTypeOfBurger < TypeOfBurger.values().length) {
                isTypeOfBurger = false;
            }
        } while (isTypeOfBurger);
        return indexTypeOfBurger;
    }

    public int inputNumberOfBurgers() {
        boolean isException = true;
        int numberOfBurgers = 0;
        do {
            System.out.println("Введите количество бургеров, которое Вы хотите заказать?");
            try {
                numberOfBurgers = myScanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Введён неверный символ. Необходимо ввести целое число!");
                myScanner.next();
            }
            if (numberOfBurgers <= 0) {
                System.out.println("Необходимо ввести число бургеров больше ноля!");
            }
            if (numberOfBurgers > 0) {
                isException = false;
            }
        } while (isException);
        return numberOfBurgers;
    }

    public void printArrayListOfBurgers(ArrayList<ClassicBurger> ordersOfBurgers) {
        for (int i = 0; i < ordersOfBurgers.size(); i++) {
            System.out.println(ordersOfBurgers.get(i).toString());
            System.out.println("Стоимость бургера: " + ordersOfBurgers.get(i).getPrice());
            System.out.println("Калорийность бургера: " + ordersOfBurgers.get(i).getCalories());
        }
    }
}
