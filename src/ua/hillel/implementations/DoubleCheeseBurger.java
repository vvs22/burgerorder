package ua.hillel.implementations;

import ua.hillel.enums.AdditionToBurger;
import ua.hillel.enums.FlavorToBurger;
import ua.hillel.enums.SortOfBurger;
import ua.hillel.enums.TypeOfBurger;

import java.util.ArrayList;

import static ua.hillel.enums.AdditionToBurger.*;

public class DoubleCheeseBurger extends ClassicBurger {

    public DoubleCheeseBurger() {
        this.typeOfBurger = super.getTypeOfBurger();
        this.sortOfBurger = SortOfBurger.DOUBLECHEESE;
        ArrayList<AdditionToBurger> addsToDbl = new ArrayList<>();
        addsToDbl.add(SALAD);
        addsToDbl.add(DOUBLECHEES);
        this.additionToBurger = addsToDbl;
        ArrayList<FlavorToBurger> flavsLocal = new ArrayList<>();
        flavsLocal.add(FlavorToBurger.NULL);
        this.flavorToBurgers = flavsLocal;
    }

    public DoubleCheeseBurger(TypeOfBurger typeOfBurger) {
        this.typeOfBurger = typeOfBurger;
        this.sortOfBurger = SortOfBurger.DOUBLECHEESE;
        ArrayList<AdditionToBurger> addsToDbl = new ArrayList<>();
        addsToDbl.add(SALAD);
        addsToDbl.add(DOUBLECHEES);
        this.additionToBurger = addsToDbl;
        ArrayList<FlavorToBurger> flavsLocal = new ArrayList<>();
        flavsLocal.add(FlavorToBurger.NULL);
        this.flavorToBurgers = flavsLocal;
    }

    public DoubleCheeseBurger(TypeOfBurger typeOfBurger, ArrayList<AdditionToBurger> additionToBurgers) {
        this.typeOfBurger = typeOfBurger;
        this.sortOfBurger = SortOfBurger.DOUBLECHEESE;
        additionToBurgers.add(SALAD);
        additionToBurgers.add(DOUBLECHEES);
        this.additionToBurger = additionToBurgers;
        ArrayList<FlavorToBurger> flavsLocal = new ArrayList<>();
        flavsLocal.add(FlavorToBurger.NULL);
        this.flavorToBurgers = flavsLocal;
    }

    public DoubleCheeseBurger(TypeOfBurger typeOfBurger, ArrayList<AdditionToBurger> additionToBurgers, ArrayList<FlavorToBurger> flavorToBurgers) {
        this.typeOfBurger = typeOfBurger;
        this.sortOfBurger = SortOfBurger.DOUBLECHEESE;
        additionToBurgers.add(SALAD);
        additionToBurgers.add(DOUBLECHEES);
        this.additionToBurger = additionToBurgers;
        this.flavorToBurgers = flavorToBurgers;
    }

    public DoubleCheeseBurger(TypeOfBurger typeOfBurger, SortOfBurger sortOfBurger, ArrayList<AdditionToBurger> additionToBurger, ArrayList<FlavorToBurger> flavorToBurgers) {
        this.typeOfBurger = super.getTypeOfBurger();
        this.sortOfBurger = SortOfBurger.DOUBLECHEESE;
        additionToBurger.add(SALAD);
        additionToBurger.add(DOUBLECHEES);
        this.additionToBurger = additionToBurger;
        this.flavorToBurgers = flavorToBurgers;
    }

}
