package ua.hillel.implementations;

import ua.hillel.enums.AdditionToBurger;
import ua.hillel.enums.FlavorToBurger;
import ua.hillel.enums.SortOfBurger;
import ua.hillel.enums.TypeOfBurger;

import java.util.ArrayList;

import static ua.hillel.enums.AdditionToBurger.*;

public class GamBurger extends ClassicBurger {

    public GamBurger(TypeOfBurger small, ArrayList<AdditionToBurger> additionToBurgers, FlavorToBurger mayonnaise) {
        this.typeOfBurger = super.getTypeOfBurger();
        this.sortOfBurger = SortOfBurger.BURGER_WITH_POTATO;
        ArrayList<AdditionToBurger> addsToDbl = new ArrayList<>();
        addsToDbl.add(SALAD);
        addsToDbl.add(POTATO);
        this.additionToBurger = addsToDbl;
        ArrayList<FlavorToBurger> flavsLocal = new ArrayList<>();
        flavsLocal.add(FlavorToBurger.NULL);
        this.flavorToBurgers = flavsLocal;
    }

    public GamBurger(TypeOfBurger typeOfBurger) {
        this.typeOfBurger = typeOfBurger;
        this.sortOfBurger = SortOfBurger.BURGER_WITH_POTATO;
        ArrayList<AdditionToBurger> addsToDbl = new ArrayList<>();
        addsToDbl.add(SALAD);
        addsToDbl.add(POTATO);
        this.additionToBurger = addsToDbl;
        ArrayList<FlavorToBurger> flavsLocal = new ArrayList<>();
        flavsLocal.add(FlavorToBurger.NULL);
        this.flavorToBurgers = flavsLocal;
    }

    public GamBurger(TypeOfBurger typeOfBurger, ArrayList<AdditionToBurger> additionToBurgers) {
        this.typeOfBurger = typeOfBurger;
        this.sortOfBurger = SortOfBurger.BURGER_WITH_POTATO;
        additionToBurgers.add(SALAD);
        additionToBurgers.add(POTATO);
        this.additionToBurger = additionToBurgers;
        ArrayList<FlavorToBurger> flavsLocal = new ArrayList<>();
        flavsLocal.add(FlavorToBurger.NULL);
        this.flavorToBurgers = flavsLocal;
    }

    public GamBurger(TypeOfBurger typeOfBurger, ArrayList<AdditionToBurger> additionToBurgers, ArrayList<FlavorToBurger> flavorToBurgers) {
        this.typeOfBurger = typeOfBurger;
        this.sortOfBurger = SortOfBurger.BURGER_WITH_POTATO;
        additionToBurgers.add(SALAD);
        additionToBurgers.add(POTATO);
        this.additionToBurger = additionToBurgers;
        this.flavorToBurgers = flavorToBurgers;
    }

}
