package ua.hillel.implementations;

import ua.hillel.enums.AdditionToBurger;
import ua.hillel.enums.FlavorToBurger;
import ua.hillel.enums.SortOfBurger;
import ua.hillel.enums.TypeOfBurger;

import java.util.ArrayList;

import static ua.hillel.enums.SortOfBurger.*;

public class CheeseBurger extends ClassicBurger {

    public CheeseBurger(TypeOfBurger typeOfBurger) {
        this.typeOfBurger = typeOfBurger;
        this.sortOfBurger = CHEESE;
        ArrayList<AdditionToBurger> addsCheese = new ArrayList<>();
        addsCheese.add(AdditionToBurger.SALAD);
        addsCheese.add(AdditionToBurger.CHEESE);
        this.additionToBurger = addsCheese;
        ArrayList<FlavorToBurger> flavsLocal = new ArrayList<>();
        flavsLocal.add(FlavorToBurger.NULL);
        this.flavorToBurgers = flavsLocal;
    }

    public CheeseBurger(TypeOfBurger typeOfBurger, ArrayList<AdditionToBurger> additionToBurgers) {
        this.typeOfBurger = typeOfBurger;
        this.sortOfBurger = CHEESE;
        additionToBurgers.add(AdditionToBurger.SALAD);
        additionToBurgers.add(AdditionToBurger.CHEESE);
        this.additionToBurger = additionToBurgers;
        ArrayList<FlavorToBurger> flavsLocal = new ArrayList<>();
        flavsLocal.add(FlavorToBurger.NULL);
        this.flavorToBurgers = flavsLocal;
    }

    public CheeseBurger(TypeOfBurger typeOfBurger, ArrayList<AdditionToBurger> additionToBurgers, ArrayList<FlavorToBurger> flavorToBurgers) {
        this.typeOfBurger = typeOfBurger;
        this.sortOfBurger = CHEESE;
        additionToBurgers.add(AdditionToBurger.SALAD);
        additionToBurgers.add(AdditionToBurger.CHEESE);
        this.additionToBurger = additionToBurgers;
        this.flavorToBurgers = flavorToBurgers;
    }

    public CheeseBurger(TypeOfBurger typeOfBurger, SortOfBurger sortOfBurger, ArrayList<AdditionToBurger> additionToBurger, ArrayList<FlavorToBurger> flavorToBurgers) {
        this.typeOfBurger = super.getTypeOfBurger();
        this.sortOfBurger = CHEESE;
        ArrayList<AdditionToBurger> addsCheese = new ArrayList<>();
        addsCheese.add(AdditionToBurger.SALAD);
        addsCheese.add(AdditionToBurger.CHEESE);
        this.additionToBurger = addsCheese;
        this.flavorToBurgers = flavorToBurgers;
    }

    public SortOfBurger getSortOfBurger() {
        return sortOfBurger;
    }
}
