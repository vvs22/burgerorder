package ua.hillel.implementations;

import ua.hillel.enums.AdditionToBurger;
import ua.hillel.enums.FlavorToBurger;
import ua.hillel.enums.SortOfBurger;
import ua.hillel.enums.TypeOfBurger;

import java.util.ArrayList;

import static ua.hillel.enums.AdditionToBurger.*;
import static ua.hillel.enums.TypeOfBurger.*;

public class ClassicBurger extends Burger {
    TypeOfBurger typeOfBurger;
    SortOfBurger sortOfBurger;
    ArrayList<AdditionToBurger> additionToBurger;
    ArrayList<FlavorToBurger> flavorToBurgers;

    public ClassicBurger() {
        this.typeOfBurger = SMALL;
        this.sortOfBurger = SortOfBurger.CLASSIC;
        ArrayList<AdditionToBurger> addsLocal = new ArrayList<>();
        addsLocal.add(SALAD);
        this.additionToBurger = addsLocal;
        ArrayList<FlavorToBurger> flavsLocal = new ArrayList<>();
        flavsLocal.add(FlavorToBurger.NULL);
        this.flavorToBurgers = flavsLocal;
    }

    public ClassicBurger(TypeOfBurger typeOfBurger) {
        this.typeOfBurger = typeOfBurger;
        this.sortOfBurger = SortOfBurger.CLASSIC;
        ArrayList<AdditionToBurger> addsLocal = new ArrayList<>();
        addsLocal.add(SALAD);
        this.additionToBurger = addsLocal;
        ArrayList<FlavorToBurger> flavsLocal = new ArrayList<>();
        flavsLocal.add(FlavorToBurger.NULL);
        this.flavorToBurgers = flavsLocal;

    }

    public ClassicBurger(TypeOfBurger typeOfBurger, ArrayList<AdditionToBurger> additionToBurger) {
        this.typeOfBurger = typeOfBurger;
        this.sortOfBurger = SortOfBurger.CLASSIC;
        additionToBurger.add(SALAD);
        this.additionToBurger = additionToBurger;
        ArrayList<FlavorToBurger> flavsLocal = new ArrayList<>();
        flavsLocal.add(FlavorToBurger.NULL);
        this.flavorToBurgers = flavsLocal;
    }

    public ClassicBurger(TypeOfBurger typeOfBurger, ArrayList<AdditionToBurger> additionToBurger, ArrayList<FlavorToBurger> flavorToBurgers) {
        this.typeOfBurger = typeOfBurger;
        this.sortOfBurger = SortOfBurger.CLASSIC;
        additionToBurger.add(SALAD);
        this.additionToBurger = additionToBurger;
        this.flavorToBurgers = flavorToBurgers;
    }

    public ClassicBurger(TypeOfBurger typeOfBurger, SortOfBurger sortOfBurger, ArrayList<AdditionToBurger> additionToBurger, ArrayList<FlavorToBurger> flavorToBurgers) {
        this.typeOfBurger = typeOfBurger;
        this.sortOfBurger = sortOfBurger;
        this.additionToBurger = additionToBurger;
        this.flavorToBurgers = flavorToBurgers;
    }

    @Override
    public TypeOfBurger getTypeOfBurger() {
        return typeOfBurger;
    }

    public SortOfBurger getSortOfBurger() {
        return sortOfBurger;
    }

    public ArrayList<FlavorToBurger> getFlavorToBurgers() {
        return flavorToBurgers;
    }

    public ArrayList<AdditionToBurger> getAdditionToBurger() {
        return additionToBurger;
    }

    @Override
    public void setTypeOfBurger(TypeOfBurger typeOfBurger) {
        this.typeOfBurger = typeOfBurger;
    }

    public void setSortOfBurger(SortOfBurger sortOfBurger) {
        this.sortOfBurger = sortOfBurger;
    }

    public void setAdditionToBurger(ArrayList<AdditionToBurger> additionToBurger) {
        this.additionToBurger = additionToBurger;
    }

    public void setFlavorToBurgers(ArrayList<FlavorToBurger> flavorToBurgers) {
        this.flavorToBurgers = flavorToBurgers;
    }

    @Override
    public double getPrice() {
        double result = 0;
        double priceOfAdds = 0;
        double priceOfFlavours = 0;
        result += typeOfBurger.getPriceOfBurger();
        for (int i = 0; i < getAdditionToBurger().size(); i++)
            priceOfAdds += (getAdditionToBurger().get(i) == null ? 0.0 : getAdditionToBurger().get(i).getPriceOfAdditions());
        for (int i = 0; i < getFlavorToBurgers().size(); i++)
            priceOfFlavours += (getAdditionToBurger().get(i) == null ? 0.0 : getFlavorToBurgers().get(i).getPriceOfFlavor());
        result += priceOfAdds + priceOfFlavours;
        return result;
    }

    @Override
    public int getCalories() {
        int result = 0;
        int resultOfAdds = 0;
        int resultOfFlavours = 0;
        result += typeOfBurger.getCaloriesOfBurger();
        for (int i = 0; i < getAdditionToBurger().size(); i++)
            resultOfAdds += (getAdditionToBurger().get(i) == null ? 0 : getAdditionToBurger().get(i).getCaloriesOfAdditions());
        for (int i = 0; i < getFlavorToBurgers().size(); i++)
            resultOfFlavours += (getFlavorToBurgers().get(i) == null ? 0 : getFlavorToBurgers().get(i).getCaloriesOfFlavor());
        result += resultOfAdds + resultOfFlavours;
        return result;
    }

    @Override
    public String toString() {
        String header = (getTypeOfBurger() == null ? " " : getTypeOfBurger() + " : ") +
                (getSortOfBurger() == null ? " " : getSortOfBurger() + " ");
        StringBuilder results = new StringBuilder(header);
        for (int i = 0; i < getAdditionToBurger().size(); i++) {
            if (getAdditionToBurger().get(i) != null) results.append(getAdditionToBurger().get(i));
        }
        for (int i = 0; i < getFlavorToBurgers().size(); i++) {
            if (getFlavorToBurgers().get(i) != null) results.append(getFlavorToBurgers().get(i));
        }
        return results.toString();
    }

}
