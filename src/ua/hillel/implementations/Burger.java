package ua.hillel.implementations;

import ua.hillel.enums.TypeOfBurger;

public class Burger {

    private TypeOfBurger typeOfBurger;

    public Burger() {
        this.typeOfBurger = TypeOfBurger.SMALL;
    }

    public Burger(TypeOfBurger typeOfBurger) {
        this.typeOfBurger = typeOfBurger;
    }

    public TypeOfBurger getTypeOfBurger() {
        return typeOfBurger;
    }

    public void setTypeOfBurger(TypeOfBurger typeOfBurger) {
        this.typeOfBurger = typeOfBurger;
    }

    @Override
    public String toString() {
        return (typeOfBurger == null ? " " : typeOfBurger + " : ");
    }

    public double getPrice() {
        double result = 0;
        result += (typeOfBurger == null ? 0.0 : typeOfBurger.getPriceOfBurger());
        return result;
    }

    public int getCalories() {
        int result = 0;
        result += (typeOfBurger == null ? 0 : typeOfBurger.getCaloriesOfBurger());
        return result;
    }
}
