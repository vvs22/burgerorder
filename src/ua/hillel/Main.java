package ua.hillel;

import ua.hillel.dataprovider.DataProvider;
import ua.hillel.enums.AdditionToBurger;
import ua.hillel.enums.FlavorToBurger;
import ua.hillel.implementations.*;

import java.util.ArrayList;
import java.util.Random;

import static ua.hillel.enums.TypeOfBurger.BIG;
import static ua.hillel.enums.TypeOfBurger.SMALL;

public class Main {

    public static void main(String[] args) {
        ArrayList<ClassicBurger> ordersOfBurgers = new ArrayList<>();
        DataProvider dataProvider = new DataProvider();
        dataProvider.addBurgerToOrder(ordersOfBurgers);
        dataProvider.printArrayListOfBurgers(ordersOfBurgers);
    }
}
